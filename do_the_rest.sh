#!/usr/bin/env bash

[[ -d ./rebos ]] || eval 'echo "Missing directory: rebos"; exit 1'

rebos_version=$(cat ./rebos/Cargo.toml | grep version | head --lines=1 | sed 's/ //g' | sed 's/version=//g' | sed 's/"//g')

echo "Rebos Version: $rebos_version"

rm -rf ./rebos/.git
rm -rf ./rebos/target

echo "Removed: .git & target"

mv ./rebos "./$rebos_version"

echo "Renamed: ./rebos -> ./$rebos_version"

sleep 0.5

git status
git add .

sleep 0.5

git status
git commit -m "Rebos $rebos_version"

sleep 0.5

git status
git push

echo " "
echo "Done!"
